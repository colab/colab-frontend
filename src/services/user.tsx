const URL: string = 'http://localhost:5000/users';

const headers: any = {
  'Content-Type': 'application/json'
};

const list = async () => {
  const response = await fetch(URL, headers);
  return await response.json();
};

const get = async (email: string) => {
  const response = await fetch(`${URL}/${email}`);
  return await response.json();
};

const getClasses = async (id: string) => {
  const response = await fetch(`${URL}/${id}/classes`);
  return await response.json();
};

const post = async (params: any) => {
  const response = await fetch(URL, {
    method: 'POST',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

const update = async (id: string, params: any) => {
  const response = await fetch(`${URL}/${id}`, {
    method: 'PUT',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

const remove = async (id: string) => {
  const response = await fetch(`${URL}/${id}`, {
    method: 'DELETE',
    headers
  });
  return await response.json();
};

export { list, get, getClasses, post, update, remove };
