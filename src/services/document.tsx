const url: string = 'http://localhost:5000/documents';

const headers: any = {
  'Content-Type': 'application/json'
};

const get = async (classId: string, parentId: string) => {
  const response = await fetch(`${url}/${classId}/${parentId}`);
  return await response.json();
};

const post = async (files: any, params: any) => {
  const data = new FormData();
  files.map((pic: any, index: any) => {
    data.append(index, pic);
  });
  for (let param in params) {
    data.append(param, params[param]);
  }
  const response = await fetch(url, {
    method: 'POST',
    body: data
  });
  return await response.json();
};

const postFolder = async (params: any) => {
  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

const update = async (id: string, params: any) => {
  const response = await fetch(`${url}/${id}`, {
    method: 'PUT',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

const remove = async (class_id: string, parent_id: string) => {
  const response = await fetch(`${url}/${class_id}/children/${parent_id}`, {
    method: 'DELETE',
    headers
  });
  return await response.json();
};

export { get, post, postFolder, update, remove };
