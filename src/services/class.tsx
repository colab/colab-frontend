const URL: string = 'http://localhost:5000/classes';

const headers: any = {
  'Content-Type': 'application/json'
};

const list = async () => {
  const response = await fetch(URL, headers);
  return await response.json();
};

const get = async (id: string) => {
  const response = await fetch(`${URL}/${id}`);
  return await response.json();
};

const getMessages = async (id: string) => {
  const response = await fetch(`${URL}/${id}/messages`);
  return await response.json();
};

const getEvaluations = async (id: string) => {
  const response = await fetch(`${URL}/${id}/evaluations`);
  return await response.json();
};

const post = async (params: any) => {
  const response = await fetch(URL, {
    method: 'POST',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

const update = async (id: string, params: any) => {
  const response = await fetch(`${URL}/${id}`, {
    method: 'PUT',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

const remove = async (id: string) => {
  const response = await fetch(`${URL}/${id}`, {
    method: 'DELETE',
    headers
  });
  return await response.json();
};

export { list, get, getMessages, getEvaluations, post, update, remove };
