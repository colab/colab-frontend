const URL: string = 'http://localhost:5000/evaluations';

const headers: any = {
  'Content-Type': 'application/json'
};

const get = async (id: string) => {
  const response = await fetch(`${URL}/${id}`);
  return await response.json();
};

const post = async (params: any) => {
  const response = await fetch(URL, {
    method: 'POST',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

const update = async (id: string, params: any) => {
  const response = await fetch(`${URL}/${id}`, {
    method: 'PUT',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

const remove = async (id: string) => {
  const response = await fetch(`${URL}/${id}`, {
    method: 'DELETE',
    headers
  });
  return await response.json();
};

export { get, post, update, remove };
