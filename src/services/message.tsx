const URL: string = 'http://localhost:5000/messages';

const headers: any = {
  'Content-Type': 'application/json'
};

const post = async (params: any) => {
  const response = await fetch(URL, {
    method: 'POST',
    body: JSON.stringify(params),
    headers
  });
  return await response.json();
};

export { post };
