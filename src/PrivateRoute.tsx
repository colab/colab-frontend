import React, { useContext } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { AuthContext } from './contexts/AuthContext';

type User = any;
interface PrivateRouteProps extends RouteProps {
  component: any;
}
const PrivateRoute: React.FC<PrivateRouteProps> = ({
  component: Component,
  ...rest
}) => {
  const { currentUser } = useContext<User>(AuthContext);
  return (
    <Route
      {...rest}
      render={(routerProps) =>
        !!currentUser ? (
          <Component {...routerProps} />
        ) : (
          <Redirect to={'/auth'} />
        )
      }
    ></Route>
  );
};

export default PrivateRoute;
