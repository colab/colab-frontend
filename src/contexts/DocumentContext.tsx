import React, { createContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import * as documentService from '../services/document';

type Props = {
  children: React.ReactNode;
};

export const DocumentContext = createContext({});

export const DocumentProvider = ({ children }: Props) => {
  const { classId } = useParams<any>();
  const paths = {
    currentPath: [{ name: 'Mon Drive', id: classId }],
    rows: [],
    selection: [],
    parentId: classId,
    classId: classId
  };

  const [path, setPath] = useState(paths);

  useEffect(() => {
    documentService.get(classId, classId).then((docs: any) => {
      const update_path = { ...paths };
      update_path.rows = docs;
      setPath(update_path);
    });
  }, []);

  return (
    <DocumentContext.Provider value={{ path, setPath }}>
      {children}
    </DocumentContext.Provider>
  );
};
