import React, { createContext, useState, useEffect } from 'react';
import fire from '../firebase';
import * as userService from '../services/user';

type User = {};
export const AuthContext = createContext<User>({});

type Props = {
  children: any;
};

export const AuthProvider: React.FC<Props> = ({ children }: Props) => {
  const [currentUser, setCurrentUser] = useState<any>({});
  useEffect(() => {
    fire.auth().onAuthStateChanged((user) => {
      if (user) {
        const { email } = user;
        if (email) {
          userService.get(email).then((res) => {
            setCurrentUser(res);
          });
        }
      } else {
        setCurrentUser(null);
      }
    });
  }, []);

  return (
    <AuthContext.Provider value={{ currentUser, setCurrentUser }}>
      {children}
    </AuthContext.Provider>
  );
};
