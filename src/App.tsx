import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './components/common/Navbar';
import Forum from './pages/Forum';
import Home from './pages/Home';
import Auth from './pages/Auth';
import Profile from './pages/Profile';
import Class from './pages/Class';
import StudentManagement from './pages/StudentManagement';
import EvaluationManagement from './pages/EvaluationManagement';
import PrivateRoute from './PrivateRoute';
import { AuthProvider } from './contexts/AuthContext';
import DocumentBoard from './pages/DocumentBoard';

const App: React.FC = () => {
  return (
    <Router>
      <AuthProvider>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/auth" component={Auth} />
          <PrivateRoute exact path="/profile" component={Profile} />
          <PrivateRoute exact path="/class" component={Class} />
          <PrivateRoute exact path="/forum/:classId" component={Forum} />
          <PrivateRoute
            exact
            path="/studentManagement/:classId"
            component={StudentManagement}
          />
          <PrivateRoute
            exact
            path="/evaluations/:classId"
            component={EvaluationManagement}
          />
          <PrivateRoute
            exact
            path="/document/:classId"
            component={DocumentBoard}
          />
          <Route component={Auth} />
        </Switch>
      </AuthProvider>
    </Router>
  );
};

export default App;
