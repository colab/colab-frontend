import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyBO_Zai6yh5956ko17F1Yr-tfd1TLeiKf4',
  authDomain: 'colab-drive-app.firebaseapp.com',
  databaseURL: 'https://colab-drive-app.firebaseio.com',
  projectId: 'colab-drive-app',
  storageBucket: 'colab-drive-app.appspot.com',
  messagingSenderId: '220980770914',
  appId: '1:220980770914:web:0f52abbcb6dcb235db793b'
};

const fire = firebase.initializeApp(firebaseConfig);
firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION);
export default fire;
