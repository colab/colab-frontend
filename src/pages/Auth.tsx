import React, { useContext } from 'react';
import Login from '../components/Login';
import { Redirect } from 'react-router-dom';
import { AuthContext } from '../contexts/AuthContext';
import Signup from '../components/Signup';
import '../styles/Auth.scss';

const Auth: React.FC = () => {
  const { currentUser } = useContext<any>(AuthContext);
  const handleSignup = () => {
    const elementLogin = document.getElementById('Login');
    const elementSignup = document.getElementById('Signup');
    if (elementLogin && elementSignup) {
      elementLogin.classList.toggle('visibleDiv');
      elementLogin.classList.toggle('hiddenDiv');
      elementSignup.classList.toggle('visibleDiv');
      elementSignup.classList.toggle('hiddenDiv');
    }
  };
  console.log('currentUser in auth', currentUser);
  if (currentUser) {
    return <Redirect to="/" />;
  } else {
    return currentUser == {} ? (
      <h3> Chargement.....</h3>
    ) : (
      <div className="modal-container-auth">
        <div id="Login" className="visibleDiv">
          <div className="spanCenter">
            <span onClick={handleSignup} className="onClickSwitch">
              S'inscrire
            </span>
          </div>
          <Login />
        </div>
        <div id="Signup" className="hiddenDiv">
          <div className="spanCenter">
            <span onClick={handleSignup} className="onClickSwitch">
              Se connecter
            </span>
          </div>
          <Signup />
        </div>
      </div>
    );
  }
};

export default Auth;
