import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import StudentList from '../components/StudentList';
import StudentAdd from '../components/StudentAdd';
import { RiUserSettingsLine } from 'react-icons/ri';
import * as classService from '../services/class';
import '../styles/StudentManagement.scss';

const StudentManagement: React.FC = () => {
  const { classId } = useParams<any>();
  const [currentClass, setCurrentClass] = useState<any>({ children: [] });

  useEffect(() => {
    classService.get(classId).then((res: any) => {
      if (res) {
        setCurrentClass(res);
      }
    });
  }, []);

  const add = (student: any) => {
    console.log('student', student);
    currentClass.children.push(student);
    classService.update(classId, currentClass).then((res: any) => {
      console.log('student add', res);
      if (res) {
        setCurrentClass(res);
      }
    });
  };

  const remove = (id: string) => {
    currentClass.children = currentClass.children.filter(
      (s: any) => s._id !== id
    );
    classService.update(classId, currentClass).then((res) => {
      if (res) {
        setCurrentClass(res);
      }
    });
  };
  return (
    <div className="modal-container-stud">
      <div className="modal-header-stud">
        <h3 className="modal-header-stud-title">
          <RiUserSettingsLine
            size={32}
            color={'#ff6584'}
            style={{ margin: '0px 10px', position: 'relative', top: '-3px' }}
          />
          Gestion des utilisateurs
        </h3>
        <div className="student-add">
          <StudentAdd add={add} students={currentClass.children} />
        </div>
      </div>
      <div className="student-list">
        <div className="student-list-board">
          <StudentList remove={remove} students={currentClass.children} />
        </div>
      </div>
    </div>
  );
};

export default StudentManagement;
