import React, { useState, useEffect, useContext, useRef } from 'react';
import { useParams } from 'react-router-dom';
import MessageList from '../components/MessageList';
import MessageAdd from '../components/MessageAdd';
import { MdForum } from 'react-icons/md';
import { AuthContext } from '../contexts/AuthContext';
import * as classService from '../services/class';
import * as messageService from '../services/message';
import '../styles/Forum.scss';

const Forum: React.FC = () => {
  const { classId } = useParams<any>();
  const divReader = useRef<any>(null);
  const { currentUser } = useContext<any>(AuthContext);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    classService.getMessages(classId).then((res: any) => {
      if (res) {
        setMessages(res);
      }
      divReader.current.scrollTop = divReader.current.scrollHeight;
    });
  }, []);

  const add = (comment: any) => {
    messageService
      .post({ comment, owner: currentUser, classId })
      .then((res: any) => {
        if (res) {
          res.owner = currentUser;
          const values: any = [...messages, res];
          setMessages(values);
          divReader.current.scrollTop = divReader.current.scrollHeight;
        }
      });
  };

  return (
    <div className="modal-container-forum">
      <div className="modal-header-forum">
        <h3 className="modal-header-forum-title">
          <MdForum
            size={32}
            color={'#ff6584'}
            style={{ margin: '0px 10px', position: 'relative', top: '-3px' }}
          />
          Forum
        </h3>
      </div>
      <div className="message-reader" ref={divReader}>
        <MessageList messages={messages} />
      </div>
      <div className="message-writer">
        <MessageAdd add={add} />
      </div>
    </div>
  );
};

export default Forum;
