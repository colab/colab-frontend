import '../styles/class.scss';
import React, { useEffect, useState, useContext } from 'react';
import * as userService from '../services/user';
import * as classService from '../services/class';
import ClassItem from '../components/ClassItem';
import { AuthContext } from '../contexts/AuthContext';
import ClassAdd from '../components/ClassAdd';
import { BsPlus } from 'react-icons/bs';
import { SiGoogleclassroom } from 'react-icons/si';

const Class: React.FC = () => {
  const { currentUser } = useContext<any>(AuthContext);
  const [isAdd, setAdd] = useState(false);
  const [classes, setClasses] = useState([]);
  useEffect(() => {
    if (currentUser._id) {
      userService.getClasses(currentUser._id).then((classes) => {
        setClasses(classes);
      });
    }
  }, [currentUser]);

  const add = (c: any) => {
    classService.post(c).then((res: any) => {
      if (res) {
        const results: any = [...classes, res];
        setClasses(results);
        setAdd(false);
      }
    });
  };
  const remove = (id: string) => {
    classService.remove(id).then((res) => {
      console.log(res);
      if (res) {
        setClasses(classes.filter((c: any) => c._id != id));
      }
    });
  };

  const items = classes.map((c: any) => (
    <ClassItem
      value={c}
      remove={remove}
      key={c._id}
      owner={c.owner == currentUser._id}
    ></ClassItem>
  ));
  return !isAdd ? (
    <div className="class-board">
      <div className="class-header">
        <h3 className="class-header-title">
          <SiGoogleclassroom
            size={32}
            color={'#ff6584'}
            style={{ margin: '0px 10px', position: 'relative', top: '-3px' }}
          />
          Mes classes
        </h3>
        <button className="add-btn" onClick={() => setAdd(true)}>
          <BsPlus size={36} color={'#6c63ff'} />
        </button>
      </div>
      <div className="class-container">{items}</div>
    </div>
  ) : (
    <ClassAdd add={add} user={currentUser} cancel={() => setAdd(false)} />
  );
};

export default Class;
