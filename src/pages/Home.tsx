import '../styles/home.scss';
import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import kanban from '../assets/kanban.svg';

const Home: React.FC = () => {
  return (
    <div className="content">
      <div className="content-left">
        <h1 className="slogan">Gérez vos cours avec Colab !</h1>
        <p>
          Un cours sur notre plateforme comprend : <br />
          - L'accès à un drive afin de pouvoir déposer des documents.
          <br />- Un forum d'échange entre le professeur et les étudiants
          inscrits à son cours.
        </p>

        <Link to="/class" className="link" activeClassName="link-active">
          <button className="content-btn primary"> Essayez </button>
        </Link>
      </div>
      <img src={kanban} alt="kanban" className="kanban" />
    </div>
  );
};

export default Home;
