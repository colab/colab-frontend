import React, { useState, useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { RiCalculatorLine } from 'react-icons/ri';
import { BsPlus, BsTrash } from 'react-icons/bs';
import { AuthContext } from '../contexts/AuthContext';
import EvaluationList from '../components/EvaluationList';
import EvaluationAdd from '../components/EvaluationAdd';
import * as classService from '../services/class';
import * as evaluationService from '../services/evaluation';
import '../styles/EvaluationManagement.scss';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

const EvaluationManagement: React.FC = () => {
  const { classId } = useParams<any>();
  const { currentUser } = useContext<any>(AuthContext);
  const [students, setStudents] = useState([]);
  const [currentClass, setCurrentClass] = useState<any>({});
  const [currentEval, setCurrentEval] = useState<any>({});
  const [evaluations, setEvaluations] = useState([]);
  const [isCreate, setCreate] = useState(false);
  const [isChanged, setChanged] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [columns] = useState<any>([]);
  const [data] = useState<any>([]);

  useEffect(() => {
    data.splice(0, data.length);
    columns.splice(0, columns.length);
    columns.push({ title: 'Étudiant', field: 'name', editable: 'never' });
    classService.get(classId).then((result: any) => {
      if (result) {
        setCurrentClass(result);
        result.children.map((stud: any) => {
          data.push({
            _id: stud._id,
            name: stud.firstname + ' ' + stud.lastname,
            sumnote: '0',
            sumcoeff: '0',
            avg: ''
          });
        });
        setStudents(result.children);
        classService.getEvaluations(classId).then((res: any) => {
          if (res) {
            res.map((ev: any) => {
              columns.push({
                title: (
                  <div>
                    {ev.name}
                    <br />
                    <i>
                      {ev.coefficient} (/{ev.denominator})
                    </i>
                    <br />
                    {currentUser._id == result.owner ? (
                      <button
                        className="del-btn"
                        onClick={() => {
                          setOpenDelete(true);
                          setCurrentEval(ev);
                        }}
                      >
                        <BsTrash size={15} color={'#6c63ff'} />
                      </button>
                    ) : (
                      ''
                    )}
                  </div>
                ),
                editPlaceholder: `${ev.name}`,
                field: ev._id,
                type: 'numeric',
                cellStyle: {
                  minWidth: 125
                }
              });
              ev.notes.map((note: any) => {
                data[data.map((e: any) => e._id).indexOf(note.studentId._id)][
                  ev._id
                ] = note.note;
                if (note.note != null) {
                  data[data.map((e: any) => e._id).indexOf(note.studentId._id)][
                    'sumnote'
                  ] =
                    parseFloat(
                      data[
                        data.map((e: any) => e._id).indexOf(note.studentId._id)
                      ]['sumnote']
                    ) +
                    ((note.note * 20) / ev.denominator) * ev.coefficient +
                    '';
                  data[data.map((e: any) => e._id).indexOf(note.studentId._id)][
                    'sumcoeff'
                  ] =
                    parseFloat(
                      data[
                        data.map((e: any) => e._id).indexOf(note.studentId._id)
                      ]['sumcoeff']
                    ) +
                    ev.coefficient +
                    '';
                  data[data.map((e: any) => e._id).indexOf(note.studentId._id)][
                    'avg'
                  ] =
                    '' +
                    (
                      parseFloat(
                        data[
                          data
                            .map((e: any) => e._id)
                            .indexOf(note.studentId._id)
                        ]['sumnote']
                      ) /
                      parseFloat(
                        data[
                          data
                            .map((e: any) => e._id)
                            .indexOf(note.studentId._id)
                        ]['sumcoeff']
                      )
                    ).toFixed(2);
                }
              });
            });
            columns.push({
              title: 'Moyenne',
              field: 'avg',
              editable: 'never'
            });
            setEvaluations(res);
          }
        });
      }
    });
  }, [isChanged, currentUser]);

  const create = (evaluation: any) => {
    evaluationService.post(evaluation).then((res: any) => {
      if (res) {
        setCreate(false);
        setChanged(!isChanged);
      }
    });
  };
  const remove = (id: string) => {
    evaluationService.remove(id).then((res) => {
      if (res) {
        setOpenDelete(false);
        setChanged(!isChanged);
      }
    });
  };

  const update = (newData: any) => {
    Object.keys(newData).forEach(function (key) {
      if (typeof newData[key] == 'number') {
        evaluationService.get(key).then((result: any) => {
          if (result) {
            result.notes.filter((e: any) => e.studentId._id === newData._id)
              .length > 0
              ? (result.notes[
                  result.notes
                    .map((e: any) => e.studentId._id)
                    .indexOf(newData._id)
                ].note = newData[key])
              : result.notes.push({
                  studentId: newData._id,
                  note: newData[key]
                });
            evaluationService.update(key, { _id: key, notes: result.notes });
          }
        });
      }
    });
    setChanged(!isChanged);
  };

  return !isCreate ? (
    <div className="modal-container-eval">
      <div className="modal-header-eval">
        <h3 className="modal-header-eval-title">
          <RiCalculatorLine
            size={32}
            color={'#ff6584'}
            style={{ margin: '0px 10px', position: 'relative', top: '-3px' }}
          />
          Gestion des évaluations
        </h3>
        <button className="add-btn" onClick={() => setCreate(true)}>
          <BsPlus size={36} color={'#6c63ff'} />
        </button>
      </div>
      <div className="eval-list">
        <div className="eval-list-board">
          <EvaluationList
            columns={columns}
            data={data}
            update={update}
            isTeacher={currentUser._id == currentClass.owner}
          />
        </div>
      </div>
      <Dialog
        open={openDelete}
        onClose={() => setOpenDelete(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Suppression : {currentEval.name}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Veuillez confirmer pour supprimer l'évaluation ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenDelete(false)} color="default">
            Retour
          </Button>
          <Button
            onClick={() => remove(currentEval._id)}
            color="primary"
            autoFocus
          >
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  ) : (
    <EvaluationAdd
      create={create}
      classId={classId}
      cancel={() => setCreate(false)}
    />
  );
};

export default EvaluationManagement;
