import React from 'react';
import { ImProfile } from 'react-icons/im';
import ProfileForm from '../components/ProfileForm';

const Profile: React.FC = () => {
  return (
    <div className="class-board">
      <div className="class-header">
        <h3 className="class-header-title">
          <ImProfile
            size={32}
            color={'#ff6584'}
            style={{ margin: '0px 10px', position: 'relative', top: '-3px' }}
          />
          Mon profil
        </h3>
      </div>
      <div className="profile-detail">
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <ProfileForm />
        </div>
      </div>
    </div>
  );
};

export default Profile;
