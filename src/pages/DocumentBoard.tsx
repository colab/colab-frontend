import '../styles/DocumentBoard.scss';
import React from 'react';
import { DocumentProvider } from '../contexts/DocumentContext';
import DocumentTable from '../components/documents/DocumentTable';
import DocumentBreadcrumb from '../components/documents/DocumentBreadcrumb';
import DocumentToolbar from '../components/documents/DocumentToolbar';

export default function Document() {
  return (
    <DocumentProvider>
      <div className="class-board">
        <div className="class-header">
          <DocumentBreadcrumb />
        </div>
        <DocumentToolbar />
        <div className="table">
          <DocumentTable />
        </div>
      </div>
    </DocumentProvider>
  );
}
