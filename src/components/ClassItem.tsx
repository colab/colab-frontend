import '../styles/class.scss';
import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { BsX } from 'react-icons/bs';

import { AuthContext } from '../contexts/AuthContext';
import {
  AiOutlineMessage,
  AiOutlineEye,
  AiOutlineCloud,
  AiOutlineCalculator
} from 'react-icons/ai';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

type Props = {
  value: any;
  remove: any;
  owner: boolean;
};

const ClassItem: React.FC<Props> = ({ value, remove, owner }: Props) => {
  const history = useHistory();

  const navigateTo = (url: string): any => {
    history.push(url);
  };

  const [openDelete, setOpenDelete] = useState(false);
  const createdDate = new Date(value.createdAt.slice(0, 10));
  return (
    <div className="class">
      {owner && (
        <div className="class-head">
          <button onClick={() => setOpenDelete(true)} className="remove-btn">
            <BsX size={24} color={'red'} />
          </button>
          <button
            className="share-btn"
            onClick={() => navigateTo(`/studentManagement/${value._id}`)}
          >
            <AiOutlineEye size={24} color={'mediumSeaGreen'} />
          </button>
          <button
            className="share-btn"
            onClick={() => navigateTo(`/evaluations/${value._id}`)}
          >
            <AiOutlineCalculator size={24} color={'mediumSeaGreen'} />
          </button>
        </div>
      )}
      {!owner && (
        <div className="class-head">
          <button
            className="share-btn"
            onClick={() => navigateTo(`/evaluations/${value._id}`)}
          >
            <AiOutlineCalculator size={24} color={'mediumSeaGreen'} />
          </button>
        </div>
      )}
      <div className="class-body">
        <p>Titre : {value.name}</p>
        <p>Total étudiant : {value.children.length}</p>
        <p>
          Date de création :{' '}
          {createdDate.getDate() +
            '/' +
            (createdDate.getMonth() + 1) +
            '/' +
            createdDate.getFullYear()}
        </p>
      </div>
      <div className="class-footer">
        <button
          className="drive-btn"
          onClick={() => navigateTo(`/document/${value._id}`)}
        >
          {' '}
          <AiOutlineCloud size={24} color={'red'} />{' '}
          <span style={{ color: 'red' }}>Drive</span>{' '}
        </button>
        <button
          className="forum-btn"
          onClick={() => navigateTo(`/forum/${value._id}`)}
        >
          {' '}
          <AiOutlineMessage size={24} color={'blue'} />{' '}
          <span style={{ color: 'blue' }}>Forum</span>
        </button>
      </div>
      <Dialog
        open={openDelete}
        onClose={() => setOpenDelete(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Suppression : {value.name}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Veuillez confirmer pour supprimer la classe ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenDelete(false)} color="default">
            Cancel
          </Button>
          <Button onClick={() => remove(value._id)} color="primary" autoFocus>
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ClassItem;
