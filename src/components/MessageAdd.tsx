import React, { useContext, useState } from 'react';
import { Button, FormGroup, FormControl } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { AiOutlineSend } from 'react-icons/ai';
import '../styles/Forum.scss';

type IProps = {
  add: any;
};
const MessageAdd: React.FC<IProps> = ({ add }: IProps) => {
  const { register, handleSubmit, errors, reset } = useForm();
  const onSubmit = (data: any) => {
    const { comment } = data;
    add(comment);
    reset();
  };
  return (
    <form className="form-add-comment" onSubmit={handleSubmit(onSubmit)}>
      <textarea
        className="textarea-comment"
        name="comment"
        placeholder="Ajouter un nouveau commentaire..."
        rows={3}
        ref={register({
          required: 'Message est nécessaire'
        })}
      ></textarea>
      <button type="submit" className="btn-submit-comment">
        <AiOutlineSend size={24} color={'#ff6584'} className="send" />
      </button>
    </form>
  );
};

export default MessageAdd;
