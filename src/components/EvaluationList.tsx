import React, { useState, useEffect } from 'react';
import MaterialTable, { Column } from 'material-table';

type IProps = {
  columns: any;
  data: any;
  update: any;
  isTeacher: any;
};

const EvaluationList: React.FC<IProps> = ({
  columns,
  data,
  update,
  isTeacher
}: IProps) => {
  return isTeacher ? (
    <MaterialTable
      title=""
      options={{
        rowStyle: {
          fontSize: 14
        },
        headerStyle: {
          backgroundColor: '#FAFAFA'
        },
        draggable: false,
        sorting: false
      }}
      columns={[...columns]}
      data={[...data]}
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                update(newData);
              }
            }, 600);
          })
      }}
    />
  ) : (
    <MaterialTable
      title=""
      options={{
        rowStyle: {
          fontSize: 14
        },
        headerStyle: {
          backgroundColor: '#FAFAFA'
        },
        draggable: false,
        sorting: false
      }}
      columns={[...columns]}
      data={[...data]}
    />
  );
};

export default EvaluationList;
