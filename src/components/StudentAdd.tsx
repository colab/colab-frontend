import React, { useContext, useState } from 'react';
import { MdAdd } from 'react-icons/md';
import { useForm } from 'react-hook-form';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { AuthContext } from '../contexts/AuthContext';
import * as userService from '../services/user';

type IProps = {
  add: any;
  students: any;
};

function Alert(props: any) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const StudentAdd: React.FC<IProps> = ({ add, students }: IProps) => {
  const { currentUser } = useContext<any>(AuthContext);
  const { register, handleSubmit, errors, reset } = useForm();
  const [message, setMessage] = useState<string>('');
  const [severity, setSeverity] = useState<string>('success');
  const [open, setOpen] = useState(false);

  const onSubmit = (data: any) => {
    const { email } = data;
    if (email === currentUser.email) {
      setMessage(' Vous ne pouvez pas ajouter vous même !');
      setSeverity('error');
      setOpen(true);
    } else {
      userService.get(email).then(
        (res) => {
          if (res) {
            if (students.filter((s: any) => s._id == res._id).length === 0) {
              add(res);
              setSeverity('success');
              setMessage(' Utilisateur ajouté !');
              setOpen(true);
              reset();
            } else {
              setMessage(' Utilisateur déjà inscrit !');
              setSeverity('error');
              setOpen(true);
            }
          } else {
            setMessage(' Utilisateur introuvable !');
            setSeverity('error');
            setOpen(true);
          }
        },
        (err) => {
          setMessage(' Utilisateur introuvable !');
          setSeverity('error');
          setOpen(true);
        }
      );
    }
  };

  return (
    <form className="form-add" onSubmit={handleSubmit(onSubmit)}>
      <input
        type="email"
        name="email"
        className="input"
        ref={register({
          required: 'Email est nécessaire',
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
            message: "L'email n'est pas valide !"
          }
        })}
        placeholder="Email"
      />
      <button type="submit" className="btn-submit-stud">
        <MdAdd size={24} color={'#6c63ff'} />
      </button>
      <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={() => setOpen(false)}
      >
        <Alert onClose={() => setOpen(false)} severity={severity}>
          {message}
        </Alert>
      </Snackbar>
    </form>
  );
};

export default StudentAdd;
