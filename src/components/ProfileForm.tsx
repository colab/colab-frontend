import React, { useContext, useState } from 'react';
import fire from '../firebase';
import { AuthContext } from '../contexts/AuthContext';
import { useForm } from 'react-hook-form';
import * as userService from '../services/user';
import LinearProgress from '@material-ui/core/LinearProgress';
import '../styles/Profile.scss';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const ProfileForm = () => {
  const { currentUser, setCurrentUser } = useContext<any>(AuthContext);
  const { register, handleSubmit, errors } = useForm();
  const [errorSignup, setErrorSignup] = useState(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [message, setMessage] = useState<string>('');
  const [severity, setSeverity] = useState<string>('success');
  const [open, setOpen] = useState(false);
  const updateUser = (data: any) => {
    setErrorSignup(null);
    setLoading(true);
    return fire
      .auth()
      .currentUser?.updateEmail(data.email)
      .then(() => {
        if (data.password) {
          return fire
            .auth()
            .currentUser?.updatePassword(data.password)
            .then(() => {
              delete data.password;
              userService
                .update(currentUser._id, data)
                .then((user) => {
                  setCurrentUser(user);
                  setLoading(false);
                  setSeverity('success');
                  setMessage(' Utilisateur mis à jour !');
                  setOpen(true);
                })
                .catch((err) => {
                  setCurrentUser(null);
                  setErrorSignup(err);
                  setLoading(false);
                  setSeverity('error');
                  setMessage(' Mise à jour impossible !');
                  setOpen(true);
                });
            });
        } else {
          return userService
            .update(currentUser._id, data)
            .then((user) => {
              setCurrentUser(user);
              setLoading(false);
              setSeverity('success');
              setMessage(' Utilisateur mis à jour !');
              setOpen(true);
            })
            .catch((err) => {
              setCurrentUser(null);
              setErrorSignup(err);
              setLoading(false);
              setSeverity('error');
              setMessage(' Mise à jour impossible !');
              setOpen(true);
            });
        }
      })
      .catch((err) => {
        console.log(err);
        setErrorSignup(err);
        setLoading(false);
        setSeverity('error');
        setMessage(' Mise à jour impossible !');
        setOpen(true);
      });
  };

  function Alert(props: any) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  return (
    <div className="Profile">
      {' '}
      <form onSubmit={handleSubmit(updateUser)}>
        {' '}
        <div>
          {' '}
          <label>Nom</label>
          <input
            type="text"
            className="input"
            name="lastname"
            placeholder="Nom"
            ref={register({ required: 'Le nom est nécessaire !' })}
            defaultValue={currentUser.lastname}
          />{' '}
        </div>{' '}
        {errors.lastname && (
          <p style={{ color: 'red' }}>{errors.lastname.message}</p>
        )}{' '}
        <div>
          {' '}
          <label>Prénom</label>
          <input
            type="text"
            className="input"
            name="firstname"
            placeholder="Prenom"
            ref={register({ required: 'Le prénom est nécessaire !' })}
            defaultValue={currentUser.firstname}
          />{' '}
        </div>{' '}
        {errors.firstname && (
          <p style={{ color: 'red' }}>{errors.firstname.message}</p>
        )}{' '}
        <div>
          {' '}
          <label>Email</label>
          <input
            type="email"
            className="input"
            name="email"
            placeholder="Email"
            defaultValue={currentUser.email}
            ref={register({
              required: 'Email est nécessaire',
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "L'email n'est pas valide !"
              }
            })}
          />{' '}
        </div>{' '}
        {errors.email && <p style={{ color: 'red' }}>{errors.email.message}</p>}{' '}
        <div>
          {' '}
          <label>Mot de passe</label>
          <input
            type="password"
            name="password"
            className="input"
            placeholder="Mot de passe"
            ref={register({
              minLength: { value: 6, message: 'Au moins 6 caractères !' }
            })}
          />{' '}
        </div>{' '}
        {errors.password && (
          <p style={{ color: 'red' }}>{errors.password.message}</p>
        )}{' '}
        <button type="submit" className="btn-submit" disabled={loading}>
          {' '}
          Update{' '}
        </button>{' '}
        {errorSignup && (
          <p style={{ color: 'red' }}>Mise à jour impossible !</p>
        )}{' '}
      </form>
      {loading && <LinearProgress />}
      <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={() => setOpen(false)}
      >
        <Alert onClose={() => setOpen(false)} severity={severity}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  );
};
export default ProfileForm;
