import React from 'react';
import { useForm } from 'react-hook-form';

type Props = {
  create: any;
  classId: any;
  cancel: any;
};

const EvaluationAdd: React.FC<Props> = ({ create, classId, cancel }: Props) => {
  const { register, handleSubmit, errors } = useForm();

  const submit = (data: any) => {
    data.classId = classId;
    data.notes = [];
    data.coefficient = parseInt(data.coefficient, 10);
    data.denominator = parseInt(data.denominator, 10);
    create(data);
  };

  return (
    <div className="form-class">
      <form onSubmit={handleSubmit(submit)} style={{ width: '60%' }}>
        <h3 className="title"> Ajouter une évaluation</h3>
        <div>
          <input
            style={{ margin: '20px 0' }}
            type="text"
            name="name"
            placeholder="Nom *"
            ref={register({
              required: 'Requis'
            })}
          />
          <input
            style={{ margin: '20px 0' }}
            type="number"
            name="coefficient"
            placeholder="Coéfficient *"
            ref={register({
              required: 'Requis'
            })}
          />
          <input
            style={{ margin: '20px 0' }}
            type="number"
            name="denominator"
            placeholder="Dénominateur *"
            ref={register({
              required: 'Requis'
            })}
          />
        </div>
        {errors.name && <p style={{ color: 'red' }}>{errors.name.message}</p>}
        <div className="form-btn">
          <button onClick={() => cancel()}>Annuler</button>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  );
};

export default EvaluationAdd;
