import React from 'react';
import { useForm } from 'react-hook-form';

type Props = {
  add: any;
  user: any;
  cancel: any;
};

const ClassAdd: React.FC<Props> = ({ add, user, cancel }: Props) => {
  const { register, handleSubmit, errors } = useForm();

  const submit = (data: any) => {
    data.owner = user;
    add(data);
  };

  return (
    <div className="form-class">
      <form onSubmit={handleSubmit(submit)} style={{ width: '60%' }}>
        <h3 className="title"> Ajouter une nouvelle classe</h3>
        <div>
          <input
            style={{ margin: '20px 0' }}
            type="text"
            name="name"
            placeholder="Nom *"
            ref={register({
              required: 'Requis'
            })}
          />
        </div>
        {errors.name && <p style={{ color: 'red' }}>{errors.name.message}</p>}
        <div className="form-btn">
          <button onClick={() => cancel()}>Annuler</button>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  );
};

export default ClassAdd;
