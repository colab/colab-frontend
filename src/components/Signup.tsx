import React, { useContext, useState } from 'react';
import fire from '../firebase';
import { AuthContext } from '../contexts/AuthContext';
import { useForm } from 'react-hook-form';
import * as userService from '../services/user';
import LinearProgress from '@material-ui/core/LinearProgress';
import '../styles/Signup.scss';

const Signup = () => {
  const { currentUser, setCurrentUser } = useContext<any>(AuthContext);
  const { register, handleSubmit, errors } = useForm();
  const [errorSignup, setErrorSignup] = useState(null);
  const [loading, setLoading] = useState<boolean>(false);
  const signUp = (data: any) => {
    setErrorSignup(null);
    setLoading(true);
    const { email, password } = data;
    fire
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((response) => {
        delete data.password;
        userService
          .post(data)
          .then((user) => {
            setCurrentUser(user);
            setLoading(false);
          })
          .catch((err) => {
            setCurrentUser(null);
            setErrorSignup(err);
            setLoading(false);
          });
      })
      .catch((err) => {
        setErrorSignup(err);
        setLoading(false);
      });
  };
  return (
    <div className="Signup">
      {' '}
      <form onSubmit={handleSubmit(signUp)}>
        {' '}
        <h3 className="type">Inscription</h3>{' '}
        <div>
          {' '}
          <input
            type="text"
            className="input"
            name="lastname"
            placeholder="Nom"
            ref={register({ required: 'Le nom est nécessaire !' })}
          />{' '}
        </div>{' '}
        {errors.lastname && (
          <p style={{ color: 'red' }}>{errors.lastname.message}</p>
        )}{' '}
        <div>
          {' '}
          <input
            type="text"
            className="input"
            name="firstname"
            placeholder="Prénom"
            ref={register({ required: 'Le prénom est nécessaire !' })}
          />{' '}
        </div>{' '}
        {errors.firstname && (
          <p style={{ color: 'red' }}>{errors.firstname.message}</p>
        )}{' '}
        <div>
          {' '}
          <input
            type="email"
            className="input"
            name="email"
            placeholder="Email"
            ref={register({
              required: 'Email est nécessaire',
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "L'email n'est pas valide !"
              }
            })}
          />{' '}
        </div>{' '}
        {errors.email && <p style={{ color: 'red' }}>{errors.email.message}</p>}{' '}
        <div>
          {' '}
          <input
            type="password"
            name="password"
            className="input"
            placeholder="Mot de passe"
            ref={register({
              required: 'Mot de passe est nécessaire',
              minLength: { value: 6, message: 'Au moins 6 caractères !' }
            })}
          />{' '}
        </div>{' '}
        {errors.password && (
          <p style={{ color: 'red' }}>{errors.password.message}</p>
        )}{' '}
        <button type="submit" className="btn-submit" disabled={loading}>
          {' '}
          S'inscrire{' '}
        </button>{' '}
        {errorSignup && (
          <p style={{ color: 'red' }}>Incription impossible !</p>
        )}{' '}
      </form>
      {loading && <LinearProgress />}
    </div>
  );
};
export default Signup;
