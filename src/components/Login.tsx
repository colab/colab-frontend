import React, { useContext, useState } from 'react';
import fire from '../firebase';
import { AuthContext } from '../contexts/AuthContext';
import { useForm } from 'react-hook-form';
import * as userService from '../services/user';
import LinearProgress from '@material-ui/core/LinearProgress';
import '../styles/Login.scss';

const Login: React.FC = () => {
  const { currentUser, setCurrentUser } = useContext<any>(AuthContext);
  const { register, handleSubmit, errors } = useForm();
  const [errorLogin, setErrorLogin] = useState(null);
  const [loading, setLoading] = useState<boolean>(false);

  const signIn = (data: any) => {
    setErrorLogin(null);
    setLoading(true);
    const { email, password } = data;
    fire
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((response) => {
        userService
          .get(email)
          .then((user) => {
            setCurrentUser(user);
            setLoading(false);
          })
          .catch((err) => {
            setCurrentUser(null);
            setLoading(false);
          });
      })
      .catch((err) => {
        setErrorLogin(err);
        setLoading(false);
      });
  };

  return (
    <div className="Login">
      <form onSubmit={handleSubmit(signIn)}>
        <h3> Connexion</h3>
        <div>
          <input
            type="email"
            name="email"
            className="input"
            placeholder="Email"
            ref={register({
              required: 'Email est nécessaire',
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "L'email n'est pas valide !"
              }
            })}
          />
        </div>
        {errors.email && <p style={{ color: 'red' }}>{errors.email.message}</p>}
        <div>
          <input
            type="password"
            name="password"
            className="input"
            placeholder="Mot de passe"
            ref={register({
              required: 'Mot de passe est nécessaire',
              minLength: { value: 6, message: 'Au moins 6 caractères !' }
            })}
          />
        </div>
        {errors.password && (
          <p style={{ color: 'red' }}>{errors.password.message}</p>
        )}
        <button type="submit" className="btn-submit" disabled={loading}>
          Se connecter
        </button>
        {errorLogin && <p style={{ color: 'red' }}>Connexion impossible !</p>}
      </form>
      {loading && <LinearProgress />}
    </div>
  );
};

export default Login;
