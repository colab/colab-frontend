import React, { Fragment } from 'react';
import { BsPeopleCircle } from 'react-icons/bs';
import '../styles/Forum.scss';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Typography from '@material-ui/core/Typography';

type IProps = {
  messages: any;
};
const MessageList: React.FC<IProps> = ({ messages }: IProps) => {
  function hashCode(str: any) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  function intToRGB(i: any) {
    let c = (i & 0x00ffffff).toString(16).toUpperCase();
    return '00000'.substring(0, 6 - c.length) + c;
  }

  const items = messages.map((item: any) => (
    <ListItem key={item._id} className="list-item">
      <ListItemAvatar>
        <Avatar
          style={{
            backgroundColor: `#${intToRGB(
              hashCode(item.owner.lastname + item.owner.firstname)
            )}`
          }}
        >
          {item.owner.lastname[0] + item.owner.firstname[0]}
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={`${item.owner.lastname}  ${item.owner.firstname}`}
        secondary={
          <Fragment>
            {item.comment}
            <br />
            <i style={{ float: 'right' }}>
              {new Date(item.createdAt).toLocaleString()}
            </i>
          </Fragment>
        }
      />
    </ListItem>
  ));

  return <List>{items}</List>;
};

export default MessageList;
