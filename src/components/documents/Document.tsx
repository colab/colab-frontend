import React, { useContext, useState, useEffect } from 'react';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { FcOpenedFolder } from 'react-icons/fc';
import { IconContext } from 'react-icons';
import { FileIcon, defaultStyles } from 'react-file-icon';
import { DocumentContext } from '../../contexts/DocumentContext';
import * as documentService from '../../services/document';
import '../../styles/Document.scss';

function Document(props: any) {
  const { path, setPath } = useContext<any>(DocumentContext);
  const [isSelected, setSelection] = useState(false);

  useEffect(() => {
    if (path.selection.length === 0) {
      setSelection(false);
    }
  });

  const setDocumentIcon = (type: string, name: string) => {
    if (type.includes('folder')) {
      return <FcOpenedFolder />;
    }
    const file_ext: string = name.split('.').pop()?.toLowerCase() || '';

    switch (file_ext) {
      case 'doc':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'docx':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'png':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'jpg':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'jpeg':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'pdf':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'zip':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'xls':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'xlsx':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'txt':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'ppt':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'pptx':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      case 'html':
        return (
          <FileIcon
            extension={file_ext}
            {...defaultStyles[file_ext]}
            labelUppercase={true}
          />
        );
      default:
        return <FileIcon />;
    }
  };

  const handleDoubleClick = (props: any) => {
    return (event: any) => {
      if (props.type.includes('folder')) {
        const paths = { ...path };
        paths.currentPath = [
          ...path.currentPath,
          { name: props.name, id: props.id }
        ];
        paths.parentId = props.id;
        documentService.get(path.classId, props.id).then((docs: any) => {
          paths.rows = docs;
          setPath(paths);
        });
      }
    };
  };
  const handleClick = (props: any) => {
    return (event: any) => {
      const paths = { ...path };
      console.log('docume' + paths.selection);
      if (!isSelected) {
        paths.selection.push(props.elt);
      } else {
        paths.selection.splice(paths.selection.indexOf(props.elt), 1);
      }
      setSelection(!isSelected);
      setPath(paths);
    };
  };

  const bytesToSize = (bytes: any) => {
    var sizes = ['B', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0 || bytes === '-') return '-';
    var i = Math.floor(Math.log(bytes) / Math.log(1024));
    return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
  };

  return (
    <TableRow
      hover
      key={props.key}
      onClick={handleClick(props)}
      onDoubleClick={handleDoubleClick(props)}
      selected={isSelected}
    >
      <TableCell className="icon-svg" component="th" scope="this.props">
        {setDocumentIcon(props.type, props.name)}
        {'  '}
        {props.name}
      </TableCell>
      <TableCell align="right">
        {props.owner.firstname} {props.owner.lastname}
      </TableCell>
      <TableCell align="right">{props.date}</TableCell>
      <TableCell align="right">{bytesToSize(props.size)}</TableCell>
    </TableRow>
  );
}

export default Document;
