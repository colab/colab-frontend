import React, { useState, useContext, useEffect } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import GetAppIcon from '@material-ui/icons/GetApp';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { DocumentContext } from '../../contexts/DocumentContext';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import * as documentService from '../../services/document';
import { AuthContext } from '../../contexts/AuthContext';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1)
      }
    },
    input: {
      display: 'none'
    },
    button: {
      margin: theme.spacing(1)
    }
  })
);

export default function DocumentToolbar() {
  const { path, setPath } = useContext<any>(DocumentContext);
  const { currentUser } = useContext<any>(AuthContext);
  const classes = useStyles();
  const [disableEdit, setEdit] = useState(true);
  const [disableDownload, setDownload] = useState(true);
  const [disableDelete, setDelete] = useState(true);
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [folderName, setFolderName] = useState('');
  const [newName, setNewName] = useState('');

  useEffect(() => {
    if (path.rows.length !== 0) {
      manageEditButton();
      manageDeleteButton();
      manageDownloadButton();
    } else {
      setEdit(true);
      setDelete(true);
      setDownload(true);
    }
  });

  const manageEditButton = () => {
    if (path.selection.length > 1) {
      return setEdit(true);
    } else if (path.selection.length === 0) {
      return setEdit(true);
    } else if (path.selection.length === 1) {
      return setEdit(false);
    }
  };

  const manageDeleteButton = () => {
    if (path.selection.length > 1) {
      return setDelete(true);
    } else if (path.selection.length === 0) {
      return setDelete(true);
    } else if (path.selection.length === 1) {
      return setDelete(false);
    }
  };

  const manageDownloadButton = () => {
    if (path.selection.length === 1) {
      if (
        path.rows[path.selection[0]] &&
        path.rows[path.selection[0]].type !== 'folder'
      ) {
        return setDownload(false);
      }
      return setDownload(true);
    }
    return setDownload(true);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickOpenEdit = () => {
    setNewName(path.rows[path.selection[0]].name);
    setOpenEdit(true);
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const handleClickOpenDelete = () => {
    setOpenDelete(true);
  };

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const createFolder = () => {
    let new_folder = {
      type: 'folder',
      name: folderName,
      owner: currentUser._id,
      size: '-',
      parent: path.parentId,
      class: path.classId
    };
    documentService.postFolder(new_folder).then((res) => {
      console.log(res);
      const path_context = { ...path };
      new_folder = { ...res };
      new_folder.owner = currentUser.firstname;
      path_context.rows.push(new_folder);
      setPath(path_context);
    });
    setOpen(false);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFolderName(event.target.value);
  };

  const handleChangeEdit = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewName(event.target.value);
  };

  const downloadFile = () => {
    fetch(path.rows[path.selection[0]].url)
      .then((res) => res.blob())
      .then((blob) => {
        const url = window.URL.createObjectURL(new Blob([blob]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', path.rows[path.selection[0]].name);
        document.body.appendChild(link);
        link.click();
        if (link.parentNode !== null) {
          link.parentNode.removeChild(link);
        }
      });
  };

  const deleteFile = () => {
    const path_context = { ...path };
    documentService
      .remove(path.classId, path.rows[path.selection[0]]._id)
      .then(() => {
        path_context.rows.splice(path.selection[0], 1);
        path_context.selection.splice(0, 1);
        if (path_context.rows.length === 0) {
          const paths = {
            currentPath: [{ name: 'Mon Drive', id: path.classId }],
            rows: [],
            selection: [],
            parentId: path.classId,
            classId: path.classId
          };
          setPath(paths);
        } else {
          setPath(path_context);
        }
      });
    setOpenDelete(false);
  };

  const editDocument = () => {
    const path_context = { ...path };
    path_context.rows[path.selection[0]].name = newName;
    documentService
      .update(
        path_context.rows[path.selection[0]]._id,
        path_context.rows[path.selection[0]]
      )
      .then(() => {
        path_context.selection = [];
        setPath(path_context);
      });
    setOpenEdit(false);
  };

  const handleFiles = (event: React.ChangeEvent<HTMLInputElement>) => {
    const path_context = { ...path };
    const files: any = [...event.target.files];
    const params: any = {
      name: '',
      type: 'file',
      owner: currentUser._id,
      parent: path.parentId,
      class: path.classId
    };
    documentService.post(files, params).then((docs) => {
      docs.map((doc: any) => {
        path_context.rows.push(doc);
      });
      path_context.selection = [];
      setPath(path_context);
    });
  };

  const clearInputFiles = (event: any) => {
    event.target.value = null;
  };

  return (
    <div className={classes.root}>
      <Button
        className={classes.button}
        variant="contained"
        color="primary"
        component="span"
        startIcon={<CreateNewFolderIcon />}
        onClick={handleClickOpen}
      >
        Create
      </Button>
      <input
        className={classes.input}
        id="contained-button-file"
        multiple
        type="file"
        onChange={handleFiles}
        onClick={clearInputFiles}
      />
      <label htmlFor="contained-button-file">
        <Button
          className={classes.button}
          variant="contained"
          color="default"
          component="span"
          startIcon={<CloudUploadIcon />}
        >
          Upload
        </Button>
      </label>
      <Button
        className={classes.button}
        variant="contained"
        color="default"
        component="span"
        startIcon={<EditIcon />}
        disabled={disableEdit}
        onClick={handleClickOpenEdit}
      >
        Rename
      </Button>
      <Button
        className={classes.button}
        variant="contained"
        color="default"
        component="span"
        startIcon={<GetAppIcon />}
        disabled={disableDownload}
        onClick={downloadFile}
      >
        Download
      </Button>
      <Button
        variant="contained"
        color="secondary"
        className={classes.button}
        startIcon={<DeleteIcon />}
        disabled={disableDelete}
        onClick={handleClickOpenDelete}
      >
        Delete
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create a folder</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="folder"
            label="Folder name"
            type="text"
            fullWidth
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="default">
            Cancel
          </Button>
          <Button onClick={createFolder} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openEdit}
        onClose={handleCloseEdit}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Rename</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="folder"
            label="New name"
            type="text"
            value={newName}
            fullWidth
            onChange={handleChangeEdit}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseEdit} color="default">
            Cancel
          </Button>
          <Button onClick={editDocument} color="primary">
            Edit
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openDelete}
        onClose={handleCloseDelete}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{'Remove'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            A document will be deleted. Do you really want to proceed?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDelete} color="default">
            Cancel
          </Button>
          <Button onClick={deleteFile} color="primary" autoFocus>
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
