import React, { useContext } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { DocumentContext } from '../../contexts/DocumentContext';
import * as documentService from '../../services/document';
import { GrOnedrive } from 'react-icons/gr';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > * + *': {
        marginTop: theme.spacing(2)
      }
    }
  })
);

export default function CustomSeparator() {
  const handleClick = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    event.preventDefault();
    const target = event.target as HTMLInputElement;
    const newIndex = path.currentPath
      .map((elt: any) => {
        return elt.id;
      })
      .indexOf(target.id);
    const paths = { ...path };
    paths.currentPath.length = newIndex + 1;
    paths.parentId = target.id;
    documentService.get(path.classId, target.id).then((docs: any) => {
      paths.rows = docs;
      setPath(paths);
    });
  };

  const classes = useStyles();
  const { path, setPath } = useContext<any>(DocumentContext);

  const items = path.currentPath.map((p: any, i: any) => {
    if (path.currentPath.length === i + 1) {
      if (i === 0) {
        return (
          <h3 className="class-header-title">
            <GrOnedrive
              size={32}
              color={'#ff6584'}
              style={{ margin: '0px 10px', position: 'relative', top: '-3px' }}
            />
            {p.name}
          </h3>
        );
      }
      return (
        <Typography key={p.id} color="textPrimary">
          {p.name}
        </Typography>
      );
    } else {
      if (i === 0) {
        return (
          <h3 className="class-header-title">
            <GrOnedrive
              size={32}
              color={'#ff6584'}
              style={{ margin: '0px 10px', position: 'relative', top: '-3px' }}
            />
            <Link
              id={p.id}
              color="inherit"
              href="/getting-started/installation/"
              onClick={handleClick}
            >
              {p.name}
            </Link>
          </h3>
        );
      }
      return (
        <Link
          id={p.id}
          color="inherit"
          href="/getting-started/installation/"
          onClick={handleClick}
        >
          {p.name}
        </Link>
      );
    }
  });

  return (
    <div className={classes.root}>
      <Breadcrumbs
        separator={<NavigateNextIcon fontSize="small" />}
        aria-label="breadcrumb"
      >
        {items}
      </Breadcrumbs>
    </div>
  );
}
