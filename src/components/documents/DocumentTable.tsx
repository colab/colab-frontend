import React, { useContext } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Document from './Document';
import { DocumentContext } from '../../contexts/DocumentContext';

export default function DocumentTable() {
  const { path, setPath } = useContext<any>(DocumentContext);
  const rows = path.rows;
  const sortTable = () => {
    return (a: any, b: any) => {
      if (a['type'] === 'folder' && b['type'] !== 'folder') {
        return -1;
      } else if (b['type'] === 'folder' && a['type'] !== 'folder') {
        return 1;
      } else {
        if (a['name'] < b['name']) {
          return -1;
        } else {
          return 1;
        }
      }
    };
  };
  rows.sort(sortTable());

  const items = rows.map((row: any, i: any) => (
    <Document
      id={row._id}
      key={i}
      elt={i}
      name={row.name}
      type={row.type}
      date={new Date('' + row.updatedAt).toLocaleString()}
      owner={row.owner}
      size={row.size}
    />
  ));

  return (
    <Table stickyHeader aria-label="sticky table">
      <TableHead>
        <TableRow>
          <TableCell>Nom</TableCell>
          <TableCell align="right">Propriétaire</TableCell>
          <TableCell align="right">Dernière modification</TableCell>
          <TableCell align="right">Taille du fichier</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>{items}</TableBody>
    </Table>
  );
}
