import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { RiDeleteBin2Line } from 'react-icons/ri';
import {
  AiOutlineRobot,
  AiOutlineUser,
  AiOutlineMail,
  AiOutlineDelete
} from 'react-icons/ai';

type IProps = {
  remove: any;
  students: any;
};

const StudentList: React.FC<IProps> = ({ remove, students }: IProps) => {
  const items = students.map((item: any) => (
    <TableRow key={item._id}>
      <TableCell align="center">{item.lastname}</TableCell>
      <TableCell align="center">{item.firstname}</TableCell>
      <TableCell align="center">{item.email}</TableCell>
      <TableCell align="center">
        <RiDeleteBin2Line
          size={24}
          style={{ cursor: 'pointer' }}
          color={'red'}
          id={item.id}
          onClick={() => remove(item._id)}
        />
      </TableCell>
    </TableRow>
  ));

  return (
    <Table className="tableStudents" stickyHeader>
      <TableHead>
        <TableRow>
          <TableCell align="center">
            <AiOutlineUser size={18} /> Nom
          </TableCell>
          <TableCell align="center">
            <AiOutlineRobot size={18} /> Prénom
          </TableCell>
          <TableCell align="center">
            <AiOutlineMail size={18} /> Email
          </TableCell>
          <TableCell align="center">
            <AiOutlineDelete size={18} /> Supprimer
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>{items}</TableBody>
    </Table>
  );
};

export default StudentList;
