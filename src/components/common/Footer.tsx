import '../../styles/footer.scss';
import React from 'react';

const Footer: React.FC = () => {
  return (
    <footer className="footer">
      <p>@2020 | Colab </p>
    </footer>
  );
};

export default Footer;
