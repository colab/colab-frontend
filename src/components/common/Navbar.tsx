import '../../styles/navbar.scss';
import React, { useContext } from 'react';
import { NavLink as Link } from 'react-router-dom';
import flower from '../../assets/flower.png';
import { AuthContext } from '../../contexts/AuthContext';
import fire from '../../firebase';
import { Redirect } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';

const Navbar: React.FC = () => {
  const { currentUser } = useContext<any>(AuthContext);
  const logout = () => {
    fire
      .auth()
      .signOut()
      .then(() => {
        return <Redirect to="/" />;
      });
  };
  function hashCode(str: any) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  function intToRGB(i: any) {
    let c = (i & 0x00ffffff).toString(16).toUpperCase();
    return '00000'.substring(0, 6 - c.length) + c;
  }
  return (
    <nav className="navbar">
      <h1 className="logo">
        <Link to="/" exact className="link">
          <img src={flower} alt="flower" className="flower" />
          Colab
        </Link>{' '}
      </h1>
      {currentUser && currentUser._id ? (
        <ul style={{ margin: '0px' }}>
          <li>
            <Link to="/" exact className="link" activeClassName="link-active">
              Home
            </Link>{' '}
          </li>
          <li>
            <Link to="/class" className="link" activeClassName="link-active">
              {' '}
              Classes{' '}
            </Link>
          </li>
          <li>
            <button
              style={{ padding: '2px 7px 5px 7px', borderRadius: '10px' }}
            >
              <span onClick={logout} style={{ color: 'red' }}>
                Déconnexion
              </span>
            </button>
          </li>
          <li>
            <Link to="/profile" className="link" activeClassName="link-active">
              <Avatar
                style={{
                  backgroundColor: `#${intToRGB(
                    hashCode(currentUser.lastname + currentUser.firstname)
                  )}`
                }}
              >
                {currentUser.lastname[0] + currentUser.firstname[0]}
              </Avatar>
            </Link>
          </li>
        </ul>
      ) : (
        <ul style={{ margin: '0px' }}>
          <li>
            <button
              style={{ padding: '2px 7px 5px 7px', borderRadius: '10px' }}
            >
              <Link to="/auth" className="link" activeClassName="link-active">
                {' '}
                Connexion
              </Link>
            </button>
          </li>
        </ul>
      )}
    </nav>
  );
};

export default Navbar;
